
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var hbs = require('hbs');
var moment = require('moment');
var mongoose = require('mongoose');
var config_connection = require('./app/config/connection');



// ----------------------------------------------------------------
// Routes
// ----------------------------------------------------------------
var index = require('./app/routes/index');

// ----------------------------------------------------------------
// Handelbars helper
// ----------------------------------------------------------------
hbs.registerHelper("checkedIf", function (condition) {
    return (condition) ? "checked" : "";
});
hbs.registerHelper('dateFormat', function(context, block) {
  if (moment) {
    var f = block.hash.format || "MMM Do, YYYY";
    return moment(context).format(f);
  }else{
    return context;   //  moment plugin not available. return data as is.
  };
});
hbs.registerPartials(path.join(__dirname, '/views/partials'));



// ----------------------------------------------------------------
// View engine setup
// ----------------------------------------------------------------

var app = express();
app.set('views', path.join(__dirname, '/app/views'));
app.set('view engine', 'hbs');
// app.use(logger('dev'));
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: false }));
app.use(cookieParser());
app.use(session({ 
	secret: 'keyboard cat',
	resave: true,
    saveUninitialized: true
}));
// Set public folder
app.use(express.static(path.join(__dirname, 'dist')));

//Start page
app.use('/', index);


// ----------------------------------------------------------------
// DB Connection
// ----------------------------------------------------------------

const { local_uri: dbconnection } = config_connection;
var mongoDB = dbconnection;
mongoose.Promise = global.Promise;
mongoose.connection.openUri(mongoDB, { /* options */ }).then(
  () => { console.log('- DB Connected -') },
  err => { console.log('Err: ', err) }
);


// ----------------------------------------------------------------
// Server
// ----------------------------------------------------------------

const server = app.listen(5800, () => {
  const { address, port } = server.address();
  console.log(`Listening at http://${address}:${port}`);
});

module.exports = app;

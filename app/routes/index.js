
var express = require('express');
var router = express.Router();
var path = require('path');

var hbs = require('hbs');


/* GET catches listing. */
router.get('/', 

	function(req, res, next) {

	//Successful, so render
	res.render('index', {
		title: 'Hello world!'
	});

});


module.exports = router;

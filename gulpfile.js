
var gulp = require('gulp'),
	nodemon = require('gulp-nodemon'),
	sass = require('gulp-sass'),
	livereload = require('gulp-livereload'),
	gulpSequence = require('gulp-sequence');


gulp.task('styles', function() {  
	return gulp.src('./src/styles/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./dist/styles'));
});

gulp.task('scripts', function() {  
  return gulp.src('./src/scripts/*.js')
    // .pipe(jshint('.jshintrc'))
    // .pipe(jshint.reporter('default'))
    .pipe(gulp.dest('dist/scripts/'))
    .pipe(livereload());
});

gulp.task('watch', function() {  
    livereload.listen();
    gulp.watch('./src/styles/**/*.scss', ['styles']);
    gulp.watch('./src/scripts/*.js', ['scripts']);
});


gulp.task('server',function(){  
    nodemon({
        'script': 'server.js',
        'ignore': 'src/js/*.js'
    });
});


gulp.task('serve', ['server','watch']);

gulp.task('default', function (cb) {
  gulpSequence(['styles', 'scripts'], 'serve', cb)

})
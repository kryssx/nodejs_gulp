# nodejs-gulp #

Simple set up for nodejs + mongoDB and gulp

Gulp task: gulp

Live: http://localhost:5800/

### What you get ###

* Node js
* mongoDB
* Gulp
* Nodemon
* Express
* Mongoose
* Handlebars

### Installing

```
$ yarn install
```

Start Docker -> Kitematic -> New Container -> Create 'mongo official'
Click new container 'mongo' -> Copy Access URL: [localhost:xxxxx]

Replace localhost in _config/connection.js_
```
local_uri: 'mongodb://localhost:xxxxx'
```

Lets run this!

```
$ gulp
```

Enjoy!!!
